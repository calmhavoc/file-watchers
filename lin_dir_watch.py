#!/usr/bin/python
import os, sys, pyinotify

class Watch(pyinotify.ProcessEvent):
    def __init__(self,command):
        self.command = command

    def process_IN_CREATE(self, event):
        a_file = os.path.join(event.path, event.name)
        print "in_create ",a_file
        self.run_command(self.command)
    def process_IN_DELETE(self, event):
        a_file = os.path.join(event.path, event.name)
        print "in_delete ",a_file
        self.run_command(self.command)
    def process_IN_MODIFY(self, event):
        a_file = os.path.join(event.path, event.name)
        print "in_modify ",a_file
        self.run_command(self.command)
    def process_IN_CLOSE_WRITE(self, event):
        a_file = os.path.join(event.path, event.name)
        print "close_write ",a_file
        self.run_command(self.command)
    def process_IN_ACCESS(self, event):
        a_file = os.path.join(event.path, event.name)
        print "access ",a_file
        self.run_command(self.command)
    def process_IN_CLOSE_NOWRITE(self, event):
        a_file = os.path.join(event.path, event.name)
        print "close_nowrite ",a_file
        self.run_command(self.command)
    def process_IN_ATTRIB(self, event):
        a_file = os.path.join(event.path, event.name)
        print "attrib ", a_file
        self.run_command(self.command)
#    def publish(self):
#        pass
    def run_command(self, command):
                os.system(command)
                sys.exit()
def main(file_to_watch,command_to_run):
    watcher = pyinotify.WatchManager()
    mask = pyinotify.IN_DELETE | pyinotify.IN_CREATE | pyinotify.IN_MODIFY | pyinotify.IN_CLOSE_NOWRITE \
    | pyinotify.IN_ACCESS | pyinotify.IN_CLOSE_WRITE | pyinotify.IN_ATTRIB
    my_watcher = pyinotify.Notifier(watcher, Watch(command_to_run))
    watcher_daemon = watcher.add_watch(file_to_watch, mask, rec=True)
    while True:
        try:
            my_watcher.process_events()
            if my_watcher.check_events():
                  my_watcher.read_events()
        except KeyboardInterrupt:
             my_watcher.stop()


if __name__ in '__main__':
        main(sys.argv[1], sys.argv[2])
        # main('/tmp/test', 'rm -rf /tmp/test/')