import os, sys, time, win32file, win32con


class Watcher(object):
    def __init__(self, ignore_files="", path='\defult\path', logfile=""):
        self.path=path
        self.logfile = logfile
        self.ignore_files=ignore_files
        self.created = []
        self.deleted = []
        self.updated = []
        self.r_to = []
        self.r_from = []
        self.total_file_count = ""
        self.file_mon(path)
        print self.path
        print self.logfile



    def file_mon(self, path):

        ACTIONS = {
          1 : "Created",
          2 : "Deleted",
          3 : "Updated",
          4 : "Renamed from something",
          5 : "Renamed to something"
        }

        FILE_LIST_DIRECTORY = 0x0001 # Need to properly comment this field, I don't remember what it's for

        path_to_watch = path
        hDir = win32file.CreateFile (
          path_to_watch,
          FILE_LIST_DIRECTORY,
          win32con.FILE_SHARE_READ | win32con.FILE_SHARE_WRITE,
          None,
          win32con.OPEN_EXISTING,
          win32con.FILE_FLAG_BACKUP_SEMANTICS,
          None
        )
        while 1:

          results = win32file.ReadDirectoryChangesW (
            hDir,
            1024,
            True,
            win32con.FILE_NOTIFY_CHANGE_FILE_NAME |
             win32con.FILE_NOTIFY_CHANGE_DIR_NAME |
             win32con.FILE_NOTIFY_CHANGE_ATTRIBUTES |
             win32con.FILE_NOTIFY_CHANGE_SIZE |
             win32con.FILE_NOTIFY_CHANGE_LAST_WRITE |
             win32con.FILE_NOTIFY_CHANGE_SECURITY,
            None,
            None
          )
          for action, file in results:
            f_filename = os.path.join (path_to_watch, file)
            # for item in self.ignore_files:
            #     if item in f_filename.split('\\'):
            #         print f_filename
            #         pass
            if f_filename.split('\\')[-1] in self.ignore_files:
                pass
            elif f_filename.split('\\')[:-1] in self.ignore_files:
                pass
            else:
                self.activity([action,f_filename])
                self.writer()

            


    def activity(self,data):
        action = str(data[0])
        if "1" in action:
            if data[1] not in self.created:
                self.created.append(data[1])
        elif "2" in action:
            if data[1] not in self.deleted:
                self.deleted.append(data[1])

        elif "3" in action:
            if data[1] not in self.updated:
                self.updated.append(data[1])

        elif "5" in action:
            if data[1] not in self.r_to:
                self.r_to.append(data[1])

        elif "4" in action:
            if data[1] not in self.r_from:
                self.r_from.append(data[1])
        else: pass


    def writer(self):
        try:
            file = open(self.logfile,'w+')
        except:
            file = open(self.logfile+'.bak','w+')
        print >> file, "System logger, mod time, last access, meta-change\n"
        print >> file, "Created\n"
        for item in self.created:
            try:
                print >> file, "%s,%s,%s,%s\n" % (item, self.file_time(item)[0],self.file_time(item)[1], self.file_time(item)[2])
            except:print>>file, '%s,(DELETED)\n' %item


        print >> file, "Deleted\n"
        for item in self.deleted:
            try:
                print >> file, "%s,%s,%s,%s\n" % (item, self.file_time(item)[0],self.file_time(item)[1], self.file_time(item)[2])
            except:print>>file, '%s,(DELETED)\n' %item


        print >> file, "Updated\n"
        for item in self.updated:
            try:
                print >> file, "%s,%s,%s,%s\n" % (item, self.file_time(item)[0],self.file_time(item)[1], self.file_time(item)[2])
            except:print>>file, '%s,(DELETED)\n' %item


        print >> file, "Something renamed to: \n"
        for item in self.r_from:
            try:
                print >> file, "%s,%s,%s,%s\n" % (item, self.file_time(item)[0],self.file_time(item)[1], self.file_time(item)[2])
            except:print>>file, '%s,(DELETED)\n' %item


        print >> file, "Something renamed from:\n"
        for item in self.r_to:
            try:
                print >> file, "%s,%s,%s,%s\n" % (item, self.file_time(item)[0],self.file_time(item)[1], self.file_time(item)[2])
            except:print>>file, '%s,(DELETED)\n' %item

        file.close()

        



        def file_time(self, file):
            try:
                (mode, ino, dev, nlink, uid, gid, size, atime, mtime, ctime) = os.stat(file)
                return[time.ctime(mtime), time.ctime(atime), time.ctime(atime)]
            except:pass

def help():
    print"watcher.exe -p c:\\directory\\to\\watch -o c:\\path\\to\\log [defaults to home\\log.txt] -e excludes"



if __name__ in '__main__':
    import getopt
    logfile_ = os.path.expanduser('~')+'\\logfile.txt'
    path = 'c:\\'
    ignore_files_ = ['test.log', 'system.log', logfile_.split('\\')[-1]]
    options, remainder = getopt.getopt(sys.argv[1:], 'o:p:e:h', ['output=', 'path=', 'excludes=', 'help'])
    for opt, arg in options:
        if opt in ('-h','--help'):
            sys.exit(help())
        elif opt in ('-o', '--output'):
            logfile_ = arg
        elif opt in ('-p', '--path'):
            path = arg
        elif opt in ('-e', '--excludes'):
            for item in arg.split(','):
                ignore_files_.append(item)

    watcher = Watcher(ignore_files_, path=path, logfile=logfile_)
else:
    print "what?\n"

